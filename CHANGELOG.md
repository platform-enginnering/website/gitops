# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2023-08-11)


### Features

* deploy canary argocd rollout ([6393c5c](https://gitlab.com/platform-enginnering/website/gitops/commit/6393c5ce7884a156d0bed28553ba45e74dc22e8b))
* new  feature 1.1.0 ([70044d6](https://gitlab.com/platform-enginnering/website/gitops/commit/70044d62a8c4409c90faa4eb3f9e9fda39836f48))
* new version ([4f3cc61](https://gitlab.com/platform-enginnering/website/gitops/commit/4f3cc61e4cef32bc40634aa39fe75fe4538adc81))
* set version number ... ([c3ca512](https://gitlab.com/platform-enginnering/website/gitops/commit/c3ca5120caf27046cb8a576fa0f4a20365f086f0))


### Bug Fixes

* adjust file .envs.sample ([01e0f59](https://gitlab.com/platform-enginnering/website/gitops/commit/01e0f59a23f5d65b1dfc121610d9c0aabe17a4f2))
* set correct version ([7725729](https://gitlab.com/platform-enginnering/website/gitops/commit/7725729e2e4130c31670e938d78029ca3689ad46))
